#!/bin/bash
#
# Copyright 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Only execute this script on a Brillo provisioned MinnowBoard.
# See your Brillo-MinnowBoard online information for initial provisioning and recovery.

set -e

function dir_with_file() {
    local file=${1}; shift
    local dir;
    for dir; do
        if [ -z "${dir}" ]; then continue; fi
        if [ -r "${dir}/${file}" ]; then
            echo ${dir}
            return
        fi
    done
    echo "Could not find ${file}, looked in $@" >&2
    return 1
}

LOCAL_DIR=$(dirname "${0}")
BASE_NAME=$(basename "${0}")

if [ -z $ANDROID_SERIAL ] && [ $# == 0 ]; then
    echo "Set ANDROID_SERIAL or specify the device with -s option" >&2
    echo "Usage: ${BASE_NAME} -s tcp:<MinnowBoard IP address>" >&2
    exit 1
fi

# Location of where the Brillo OS image is built.
OS=$(dir_with_file boot.img \
    "${ANDROID_PROVISION_OS_PARTITIONS}" \
    "${LOCAL_DIR}" \
    "${BRILLO_OUT_DIR}" \
    "${ANDROID_PRODUCT_OUT}")

VENDOR=$(dir_with_file bootloader \
    "${ANDROID_PROVISION_VENDOR_PARTITIONS}" \
    "${LOCAL_DIR}" \
    "${ANDROID_BUILD_TOP}/vendor/bsp/intel/minnowboard/boot_binaries")

if [ -x "${LOCAL_DIR}"/fastboot ]; then
  FASTBOOT="${LOCAL_DIR}"/fastboot
else
  FASTBOOT=fastboot
fi

#
# Due to possible changes in the u-boot version, we might have some
# issues flashing gpt or recognizing the _a/_b partitions.  If we do
# have a failure, we must first re-flash bootloader and reboot to the
# updated version
set +e
fastboot flash gpt "${OS}"/partition-table.img "$@"
bootloader_rc=$?
fastboot flash boot_a "${OS}"/boot.img "$@"
boota_rc=$?
if [ $bootloader_rc -ne 0 -o $boota_rc -ne 0 ]; then
    fastboot flash bootloader "${VENDOR}"/bootloader \
             reboot bootloader \
             "$@"
fi

set -e
${FASTBOOT} \
    flash gpt        "${OS}"/partition-table.img \
    flash bootloader "${VENDOR}"/bootloader \
    flash boot_a     "${OS}"/boot.img \
    flash boot_b     "${OS}"/boot.img \
    flash system_a   "${OS}"/system.img \
    flash system_b   "${OS}"/system.img \
    flash userdata   "${OS}"/userdata.img \
    flash oemvars    "${VENDOR}"/oemvars.txt \
    set_active _a \
    "$@"

echo "Please run the following command to restart your MinnowBoard"
echo "		${FASTBOOT} $@ reboot"
echo
